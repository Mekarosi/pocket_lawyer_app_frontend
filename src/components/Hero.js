import React from 'react'
import pl_image1 from '../images/pl_image1.PNG'
import pl_image2 from '../images/pl_image2.PNG'

const Hero = () => {
    return (
      <div className="hero-main">
        <div className="hero-nav">
            <div className="hero-logo">
              <img src={pl_image1} alt='logo'/>
            </div>
            <nav>
              <ul>
                <li>Company</li>
                <li>Services</li>
                <li>Pricing</li>
                <li>Resources</li>
                <li>FAQ</li>
              </ul>  
          </nav>
          <button className="hero-button">LOGIN</button>
        </div>
        <div className="hero-search">
          <div className="hero-search-text">
          <p>Find out about the newly updated resources. <span>Check out</span></p>
          </div>
          <div className="hero-search-x">
            <h2>X</h2>
          </div>
          
        </div>
        <div className="hero-inner">
          <div className="hero-main-left">
             <h3 className="hero-main-h3">Lawyers In</h3><br/>
             <h3 className="hero-main-h3-2nd">Your <span className="hero-main-h3-span">Pocket</span></h3>
             
             <p className="hero-inner-text">Africa's first fully integrate legal tech startup that offers <br/>access to affordable premium legal services and solutions to <br/> SMEs and Startups.</p>
             <div classname="hero-inner-button-content">
              <button type='button' className="inner-button">Get A Free Account</button>
              <span className="button-side-text">Get in Touch</span>
             </div>
          </div>
          <div className="hero-main-right">
            <img src={pl_image2}/>
          </div>
        </div>
      </div>
    );
  }
  
  export default Hero;