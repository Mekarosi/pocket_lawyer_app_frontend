import React from 'react'
import pl_image9aa from '../images/pl_image9aa.PNG'
import pl_image9ab from '../images/pl_image9ab.PNG'
import pl_image9 from '../images/pl_image9.PNG'
import pl_image10 from '../images/pl_image10.PNG'
import pl_image11 from '../images/pl_image11.PNG'
import pl_image12 from '../images/pl_image12.PNG'





const OurOffer = () => {
    return (
      <div className="our-offer">
        <div className='our-offer-text-items'>
        <div className='our-offer-text'>
          <h6 className='our-offer-h6'>What we Offer</h6>
          <h3 className='our-offer-h3'>Explore Our Retail Solutions</h3>
        </div>
        <div className='our-offer-arrows'>
          <img  src={pl_image9aa} alt="navigation arrow left" />
          <img  src={pl_image9ab} alt="navigation arrow right" />
        </div>
        </div>
        <div className="our-offer-card">
          <div className="our-offer-card-item">
            <img  src={pl_image9} alt="" className="our-offer-card-image"/>
            <h5 className="our-offer-card-title">Register a Business</h5>
            <p className="our-offer-card-desc">Register A Busines Entity is the First Step to Turing Your Idea Into An Actual Business. Lets's Help You Start This Journey.</p>
            <button className="our-offer-card-button">Explore</button>
          </div>
          <div className="our-offer-card-item">
          <img  src={pl_image10} alt="" className="our-offer-card-image"/>
            <h5 className="our-offer-card-title">Post registration matters</h5>
            <p className="our-offer-card-desc">Sorting Out Those In House Issues Are Essential To Any Thriving Business. We Are More Than Happy To Help. Shall We?!</p>
            <button className="our-offer-card-button">Explore</button>
          </div>
          <div className="our-offer-card-item">
          <img  src={pl_image11} alt="" className="our-offer-card-image"/>
            <h5 className="our-offer-card-title">Intellectual Property Protection</h5>
            <p className="our-offer-card-desc">Did You Know That Protecting Your Intellectual Property Could Enhance The Market Value Of Your Business. We Can Help You!</p>
            <button className="our-offer-card-button">Explore</button>
          </div>
          <div className="our-offer-card-item">
          <img  src={pl_image12} alt="" className="our-offer-card-image" />
            <h5 className="our-offer-card-title">Staff onboarding and outboarding</h5>
            <p className="our-offer-card-desc">Employing Staff. Whatever Type And Terms. Without Proper Documentation Is A Recipe For Internal Combustion And Finally Disaster! Let's Help!</p>
            <button className="our-offer-card-button">Explore</button>
          </div>
        </div>
        <button className="our-offer-card-button2">See All Packages</button>
      </div>
    );
  }
  
  export default OurOffer;