import React from 'react'
import pl_image22 from '../images/pl_image22.PNG'
import pl_image26 from '../images/pl_image26.PNG'
import pl_image27 from '../images/pl_image27.PNG'
import pl_image28 from '../images/pl_image28.PNG'

const Footer = () => {
    return (
      <div className="footer-main">
        <div className='footer-main-inner'>
        <div className='footer-main-inner-items'>
          <img src={pl_image22} alt='logo'/>
          <div>
            <p className='footer-main-inner-item-p'>Suit 1,319, Borno Way <br/> off Herbert Macaulay Way<br/> Yaba, Lagos, Nigeria</p>
          </div>
          <div>
            <p className='footer-main-inner-item-p1'>Email:</p>
            <p className='footer-main-inner-item-p'>hello@pocketLawyer.io</p>
          </div>
          <div>
            <p className='footer-main-inner-item-p1'>Call:</p>
            <p className='footer-main-inner-item-p'>+234 916 803 6060</p>
            
          </div>

        </div>
      
        <div>
        <h5 className='footer-main-inner-item-h5'>Company</h5>
          
            <div className='footer-main-inner-item-p'>About</div>
            <div className='footer-main-inner-item-p'>Pricing</div>
            <div className='footer-main-inner-item-p'>Service</div>
            <div className='footer-main-inner-item-p'>FAQ</div>
            <div className='footer-main-inner-item-p'>help Center</div>
          
        </div>

        <div>
        <h5 className='footer-main-inner-item-h5'>Useful Links</h5>
        
            <div className='footer-main-inner-item-p'>Press</div>
            <div className='footer-main-inner-item-p'>Become a PL Affillate</div>
            <div className='footer-main-inner-item-p'>General terms & conditions</div>
            <div className='footer-main-inner-item-p'>Payment terms & conditions</div>
            <div className='footer-main-inner-item-p'>Help Center</div>
          
        <div></div>
        </div>
        <div>
          <h5>
            
          </h5>
          <div className='footer-email'>
          <h5 className='footer-main-inner-item-h5'>Subscribe</h5>
            <p className='footer-email-p'>Enter your Email</p>
            <button className='footer-email-inner-button'>subscribe</button>
          </div>
          <h5 className='footer-main-inner-item-h5'>Follow</h5>
          <img src={pl_image27} alt='facebook'/>
          <img src={pl_image28} alt='Instagram'/>
          <br/>
          <img src={pl_image26} alt='twitter'/>
            
        </div>
        </div>
       <div>
        <hr/>
       </div>
       <div className='footer-legal'>
          <div className='footer-legal-outer'>2022 Pocket Lawyers. All rights reserved</div>
            <div className='footer-legal-side'>
            <div className='footer-legal-side-inner'> Term of service </div>
            <div>Privacy policy</div>
            </div>
       </div>
         
      </div>
    );
  }
  
  export default Footer;