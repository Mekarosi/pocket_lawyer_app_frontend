import React from 'react'
import pl_image13 from '../images/pl_image13.PNG'
import pl_image14 from '../images/pl_image14.PNG'

const PlWhy = () => {
    return (
      <div className="pl_why_main">
        <div className="pl_why_text">
            <h6 className="pl_why_text-h6">Why PocketLawyers.io</h6>
            <h4 className="pl_why_text-h4">A Virtual Law Firm Poised To Democratize <br/> Access To Premium Legal Solutions <br/> For SMEs And Startupa.</h4>
        </div>
        <div  className="pl_why_card-container" >
            <div className="pl_why_card">
                <img src={pl_image13} alt=''/>
                <h6>Pre-Approved Lawyers</h6>
                <p>You have access to premuim<br/> Lawyers you can trust.</p>
            </div>
            <div className="pl_why_card">
                <img src={pl_image13} alt=''/>
                <h6>Resources and Templates</h6>
                <p>We provide businesses with free and<br/>premiun reports, books, journals and<br/>document</p></div>
            <div className="pl_why_card">
                <img src={pl_image13} alt=''/>
                <h6>Confidential and Private</h6>
                <p>All matters shared with<br/>PocketLawyers.io are kept highly<br/> secure and private.</p>
            </div>
        </div>
        <div className='pl_why_container'>
            <div className='pl_why_container-text'>
                <div className='pl_why_container-text-h6'>We Fit Into Your Pocket. No Matter Your Budget.</div>
                <div className='pl_why_container-text-h3'>Choose Your Retainer Package</div>
            </div>
            <div className='pl_why_container-card'>
                <div className='pl_why_container-card1'>
                    <img src={pl_image14} alt=''/>
                    <h6 className='pl_why_container-card1-title'>Small Scale Business</h6>
                    <p className='pl_why_container-card1-desc'>Viverra Amet, Alquam Pretium<br/>Consequat. Viverra Amet, Allquam<br/>Pretium Consequat.</p>
                    <button className='our-offer-card-button'>Explore</button>
                </div>
                <div className='pl_why_container-card1'>
                    <img src={pl_image14} alt=''/>
                    <h6 className='pl_why_container-card1-title'>Starp Ups</h6>
                    <p className='pl_why_container-card1-desc'>This Service is Advised For Startups That Want<br/>To Outsource Their Legal Departments.</p>
                    <button className='our-offer-card-button'>Explore</button>
                </div>
                
            </div>
            <button className='our-offer-card-button2'>Explore Packages</button>
        </div>
      </div>
    );
  }
  
  export default PlWhy;