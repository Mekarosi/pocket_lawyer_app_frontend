import React from 'react'
import pl_image3 from '../images/pl_image3.PNG'
import pl_image4 from '../images/pl_image4.PNG'
import pl_image5 from '../images/pl_image5.PNG'
import pl_image6 from '../images/pl_image6.PNG'
import pl_image7 from '../images/pl_image7.PNG'
import pl_image8 from '../images/pl_image8.PNG'

const Testimonies = () => {
    return (
      <div className="testimonies">
        <div className="testimonnies-text" >
          <p className="testimonnies-text1">All legal needs, sorted</p>
          <h4 className="testimonnies-text2">Satisfied StartUps & Businesses</h4>
        </div>
        <div>
          <ul>
            <li><img src={pl_image3} alt="testimonies image1" /></li>
            <li><img src={pl_image4} alt="testimonies image2" /></li>
            <li><img src={pl_image5} alt="testimonies image3" /></li>
            <li><img src={pl_image6} alt="testimonies image4"/></li>
            <li><img src={pl_image7} alt="testimonies image5" /></li>
            <li><img src={pl_image8} alt="testimonies image6" /></li>
          </ul>
        </div>
      </div>
    );
  }
  
  export default Testimonies;