import React from 'react'
import pl_image17 from '../images/pl_image17.PNG'
import pl_image18 from '../images/pl_image18.PNG'
import pl_image19 from '../images/pl_image19.PNG'
import pl_image20 from '../images/pl_image20.PNG'
import pl_image21 from '../images/pl_image21.PNG'


const Feedback = () => {
    return (
      <div className="clientreview-main">
        <div className="clientreview-inner">
            <div className="clientreview-inner1">
                <h5 className='pl_why_container-text-h6'>Clients Feedback</h5>
                <h3 className='testimonnies-text2'>What People Are Saying About Us</h3>
            </div>
            <div className="clientreview-inner2">
                <div className="clientreview-inner2-card1">
                    <div className="clientreview-inner2-card1-outer">
                        <img src={pl_image17} alt="A man's face" />
                        <div className="clientreview-inner2-card-name-group">
                            <div className="clientreview-inner2-card-name">Adeola Odekun</div>
                            <div className="clientreview-inner2-card-job">Business Analyst</div>
                        </div>
                    </div>
                    <div className="clientreview-inner2-card1-inner">
                        <img src={pl_image20} alt="quotation mark"/>
                        <p className="clientreview-inner2-card2-desc">We appreciate the way you handle all <br/>our legal ignorance with patience and <br/>how you systematically help set a <br/>structure that safeguard business.</p>
                    </div>
                </div>
                <div className="clientreview-inner2-card2">
                    <div className="clientreview-inner2-card2-outer">
                        <img src={pl_image18} alt="A woman's face" />
                        <div className="clientreview-inner2-card-name-group">
                            <div className="clientreview-inner2-card-name">Ngozi Nwabueze</div>
                            <div className="clientreview-inner2-card-job">Brand Strategist</div>
                        </div>
                    </div>
                    <div className="clientreview-inner2-card2-outer">
                        <img src={pl_image21} alt="quotation mark"/>
                        <p className="clientreview-inner2-card2-desc">Every Sailor needs a compass to safely <br/>sail the seas, pocket Lawyers have been <br/>our compass since incestion guiding us <br /> with a lot of patience and <br/>professionalism.
                        </p>
                    </div>
                </div>
                <div className="clientreview-inner2-card1">
                 <div className="clientreview-inner2-card1-outer">
                        <img src={pl_image19} alt="A man's face"/>
                        <div className="clientreview-inner2-card-name-group">
                            <div className="clientreview-inner2-card-name">Saheed Osupa</div>
                            <div className="clientreview-inner2-card-job">Startup Founder</div>
                        </div>
                    </div>
                    <div className="clientreview-inner2-card1-inner">
                        <img src={pl_image20} alt="quotation mark"/>
                        <p className="clientreview-inner2-card2-desc">We appreciate the way you handle all <br/>our legal ignorance with patience and <br/>how you systematically help set a <br/>structure that safeguard business.</p>
                    </div>
                </div>
            </div>
        </div>
      </div>
    );
  }
  
  export default Feedback;