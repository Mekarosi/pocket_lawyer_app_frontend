import React from 'react'
import pl_image15 from '../images/pl_image15.PNG'
import pl_image16 from '../images/pl_image16.PNG'

const Faqs = () => {
    return (
      <div className="faqs-main">
        <div className="faqs-main-first">
        <div className="faqs-main-outer">
            <h4 className='our-offer-card-title'>Frequently Asked Questions</h4>
            <h4 className='faqs-main-outer-title'>FAQS</h4>
        </div>
        <div className="faqs-main-inner">
            <div className="faqs-main-inner1">
                <img src={pl_image16} alt='Plus sign'/>
                <h5>Who are We?</h5>  
            </div>
            <hr />
            <div className="faqs-main-inner1">
               <img src={pl_image16} alt='Plus sign' />
                <h5>Why PocketLawyer?</h5>
            </div>
            <hr />
            <div className="faqs-main-inner1">
                <img src={pl_image16} alt='Plus sign' />
                <h5>Where can our services be accessed?</h5>
            </div>
            <hr />
            <div className="faqs-main-inner1">
               <img src={pl_image16} alt='Plus sign' />
               <h5>Where are you located?</h5>
            </div>
            <hr />
            <div className="faqs-main-inner1">
               <img src={pl_image16} alt='Plus sign' />
                <h5>What are your business hours?</h5>
            </div>
            <hr />
            <div className="faqs-main-inner1">
                <img src={pl_image16} alt='Plus sign' />
                <h5>Will the information I share be confidential?</h5>
            </div>
            <hr />
            <div className="faqs-main-inner1">
                <img src={pl_image16} alt='Plus sign' />
                <h5>Do you offer Startup Advisory?</h5>
            </div>
            <hr />
            
        </div>
          <a  href='#'className='faqs-main-inner1-link'>See More</a>
        </div>
        <div className="faqs-main-first">
            <img src={pl_image15} alt='Lady standing' />
        </div>
       
      </div>
    );
  }
  
  export default Faqs;