import Hero from './components/Hero'
import Testimonies from './components/Testimonies'
import OurOffer from './components/OurOffer'
import PlWhy from './components/PlWhy'
import Faqs from './components/Faqs'
import Feedback from './components/Feedback'
import Footer from './components/Footer'

import './App.css';

function App() {
  return (
    <div className="App">
      <Hero />
      <Testimonies />
      <OurOffer />
      <PlWhy />
      <Faqs />
      <Feedback />
      <Footer />
    </div>
  );
}

export default App;
